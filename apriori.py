#!/usr/bin/env python2.7

import sys
import os
import re

def nonBlank(value):
   return value != ''

def getTransactions(fname):
   result = []
   file = open(fname, 'rU')
   lines = file.readlines()
   i =1
   for line in lines:
      line =line.strip()
      words = line.split(',')
      words = sorted(list(set(filter(nonBlank, words))))
      result.append(words)
      #537 of these cases
      #if len(words) < 10:
      #   print("%d. %d\n" % (i, len(words)))
      #   i=i+1
      #print(words)
   file.close()
   return result
   

def initPass(transactions):
   candidates = {}
   for trans in transactions:
      for item in trans:
         curItem = tuple([item])
         if curItem in candidates.keys():
            candidates[curItem] += 1
         else:
            candidates[curItem] = 1
   return candidates


def candidateGen(frequentItemsets, size):
   candidates = {}
   for freq1 in frequentItemsets:
      for freq2 in frequentItemsets:
         if freq1[0:-1] == freq2[0:-1] and freq1[-1] < freq2[-1]:
            curCand = freq1 + tuple([freq2[-1]])
            candidates[curCand] = 0
            candList = list(curCand)
            for item in candList:
               subCand = candList[:]
               subCand.remove(item)
               subCand = tuple(subCand)
               if subCand not in frequentItemsets:
                  del candidates[curCand]
                  break
   #print(candidates)
   return candidates

  
#def genRules(frequent, allFrequent, minconf, numTransactions):
#   rules = []
#   for ruleTuple in frequent.keys():
#      for firstPart in allFrequent.keys():
#         for secondPart in allFrequent.keys():
#            ruleset = set(ruleTuple)
#            rulePart1 = set(firstPart)
#            rulePart2 = set(secondPart)
#            if rulePart1 != rulePart2 and len(rulePart1) < len(ruleset) \
#              and len(rulePart2) < len(ruleset) and rulePart1.issubset(ruleset) \
#              and rulePart2.issubset(ruleset) and not rulePart1.issubset(rulePart2) \
#              and not rulePart2.issubset(rulePart1) and len(rulePart1.intersection(rulePart2)) == 0:
#               rule1Support = (float(frequent[ruleTuple])/numTransactions)/(float(allFrequent[firstPart])/numTransactions)
#               rule2Support = (float(frequent[ruleTuple])/numTransactions)/(float(allFrequent[secondPart])/numTransactions)
#               rule1 = [list(rulePart1), list(rulePart2)]
#               rule2 = [list(rulePart2), list(rulePart1)]
#               if rule1Support >= minconf and rule1 not in rules:
#                  rules.append(rule1)
#               if rule2Support >= minconf and rule2 not in rules:
#                  rules.append(rule2)
#   return rules
    

def genRules(frequentSets, allFrequent, minconf, numTransactions):
   ruleList = list()
   for itemset in frequentSets:
      hList = list()
      if len(itemset) >= 2:
         itemList = list(itemset)
         for item in itemList:
            conSet = itemList[:]
            conSet.remove(item)
            confidence = float(frequentSets[itemset])/float(allFrequent[tuple(conSet)])
            
            if confidence >= minconf:
               ruleList.append([list(conSet), list([item])])
               hList.append(tuple([item]))
         ruleList += apGenRules(itemset, frequentSets, allFrequent, hList, 1, minconf)

   return(ruleList)


def apGenRules(itemset, frequentSets, allFrequent, hList, size, minconf):
   ruleList = list()
   m = size + 1
   if len(itemset) > m and len(hList) != 0:
      new_hList = candidateGen(hList, m).keys()
      for consequents in new_hList:
         conList = list(itemset[:])
         for element in consequents:
            conList.remove(element)
         confidence = float(frequentSets[itemset])/float(allFrequent[tuple(conList)])
         
         if confidence >= minconf:
            ruleList.append([list(conList), list(consequents)])
         else:
            new_hList.remove(consequents)
      ruleList += apGenRules(itemset, frequentSets, allFrequent, new_hList, m, minconf)
   
   return ruleList


def print_rules(ruleList):
   for rule in ruleList:
      print_rule = ''
      for item in rule[0]:
         print_rule += str(item) + ', '  
      
      print_rule = print_rule[:-2]
      print_rule += ' --> '

      for item in rule[1]:
         print_rule += str(item) + ', '
      
      print_rule = print_rule[:-2]
      print(print_rule)


def apriori(transactions, minsup, minconf):
   candidInit = initPass(transactions)
   allFrequent = {}
   ruleFrequent = {}
   frequentK = {key:value for key, value in candidInit.items() if float(value)/float(len(transactions)) >= minsup}
   allFrequent = dict(frequentK.items() + allFrequent.items())
   frequentLast = frequentK
   i = 2
   while len(frequentK) != 0:
      frequentLast = frequentK
      candidK = candidateGen(frequentLast.keys(), i)
      for trans in transactions:
         for cand in candidK.keys():
            candSet = set(cand)
            curTrans = set(trans)
            if candSet.issubset(curTrans):
               candidK[cand] += 1
      frequentK = {key:value for key, value in candidK.items() if float(value)/float(len(transactions)) >= minsup}
      allFrequent = dict(frequentK.items() + allFrequent.items())
      ruleFrequent = dict(frequentK.items() + ruleFrequent.items())
      i+=1
   #use allFrequent, ruleFrequent, and frequentK to generate rules at this point
   #print(ruleFrequent)
   rules = genRules(ruleFrequent, allFrequent, minconf, float(len(transactions)))
   return rules
   
   
def isFloat(value):
   try:
      a = float(value)
   except ValueError:
      return False
   else:
      return True

def main():
   minsup = 0.0
   minconf = 0.0
   if len(sys.argv) != 4:
      print("Usage: apriori.py file.csv minsup minconf")
   elif not os.path.exists(sys.argv[1]):
      print("%s doesn't exist" % sys.argv[1]) 
   elif not isFloat(sys.argv[2]) or not isFloat(sys.argv[3]):
      print("minsup and minconf must be numeric values")
   elif float(sys.argv[2]) < 0.0 or float(sys.argv[2]) > 1.0:
      print("minsup must be in range 0 to 1 inclusive")
   elif float(sys.argv[3]) < 0.0 or float(sys.argv[3]) > 1.0:
      print("minconf must be in range 0 to 1 inclusive")
   else:
      minsup = float(sys.argv[2])
      minconf = float(sys.argv[3])
      transactions = getTransactions(sys.argv[1])
      rules = apriori(transactions, minsup, minconf)
      print_rules(rules)
   


if __name__ == '__main__':
   main()

