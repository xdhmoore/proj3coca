#!/usr/bin/env python3

import glob
import os
import csv
from nltk.corpus import stopwords

COCA_DIR = '/lib/466/COCA/text_unzipped/'
DATA_DIR = 'rules/'

STOPWORDS = stopwords.words('english')

def format_line(line):
    line = [word.lower() for word in line.split(' ') if word.isalnum()]
    line = list(set(line) - set(STOPWORDS))

    return line


def main():
    coca_files = sorted([fname for fname in os.listdir(COCA_DIR) if 'news' in fname])
    
    print(coca_files)
     
    for text in coca_files:
        if '201' in text[:4]:
            writefile = 'rule_' + text[:4] + '_news.csv'
        else:
            writefile = 'rule_' + text.split('.')[0].split('_')[2] + '_news.csv'
        print(writefile)
         
        with open(DATA_DIR + writefile, 'w', encoding='utf8', newline='') as wf:
            csv_writer = csv.writer(wf)

            with open(COCA_DIR + text, 'r') as rf:
                for row in rf:
                    textLine = ' '.join(row.split(' ')[1:])
                    textLine = textLine.replace(' @ @ @ @ @ @ @ @ @ @ ', ' ')
                    textLine = textLine.replace(' <p> ', ' ')                    

                    sentences = textLine.split(' . ')
                    #paragraphs = textLine.split(' <p> ')

                    for sentence in sentences:
                        sentence = format_line(sentence)
                        if len(sentence) >= 7:
                            csv_writer.writerow(sentence)

 
if __name__ == '__main__':
    main()
