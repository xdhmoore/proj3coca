import numpy as np
import pandas as pd
import nltk
import re
import os
import codecs
from sklearn import feature_extraction
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.cluster import KMeans
from nltk.stem.snowball import SnowballStemmer
import csv

sents = []
types = []
years = []

# borrowed from http://brandonrose.org/clustering

def tokenize_and_stem(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in tokens:
        if re.search('[0-9]', token):
            filtered_tokens.append(token)
    stems = [stemmer.stem(t) for t in filtered_tokens]
    return stems

# done borrowing

def strip_years(sentence):
	return re.findall(r'(\d{4})', sentence)

stemmer = SnowballStemmer("english")

with open("/home/jacobs466/finalProject/data/coca_full_table.csv", "r") as f:
	reader = csv.reader(f)
	i = 0
	for row in reader:
		if i % 100 == 0:
			years_mentioned = strip_years(row[3])
			if len(years_mentioned) > 0:
				sents.append(' '.join(years_mentioned))
				types.append(row[1])
				years.append(int(row[2]))
		i += 1

print(len(sents))

tfidf_vec = TfidfVectorizer(max_df=0.8, max_features=200000, min_df=0.01, stop_words='english', use_idf=True, tokenizer=tokenize_and_stem, ngram_range=(1,1))
tfidf_mat = tfidf_vec.fit_transform(sents)

print(tfidf_mat.shape)

features = tfidf_vec.get_feature_names()
distance = 1 - cosine_similarity(tfidf_mat)

kmeans = KMeans(n_clusters=22)
kmeans.fit(tfidf_mat)
clusters = kmeans.labels_.tolist()
print(features)
print(clusters)
print(len(clusters))

pd_docs = {'years': years, 'types': types, 'sents': sents, 'clusters': clusters}
pd_frame = pd.DataFrame(pd_docs, index=[clusters], columns=['years', 'types', 'sents', 'clusters'])

print(pd_frame['clusters'].value_counts())

year_cluster = pd_frame['years'].groupby(pd_frame['clusters'])
type_cluster = pd_frame['types'].groupby(pd_frame['clusters'])
print(year_cluster.mean())
print(type_cluster.value_counts())

cents = kmeans.cluster_centers_.argsort()[:, ::-1]

for i in range(len(cents)):
	print("Cluster",i)
	
	for j in cents[i, :3]:
		print(features[j])
	
	print()
