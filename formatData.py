#!/usr/bin/env python3

import os
import re
import csv
import string
import codecs

'''
This program reads in the COCA sample data and stores the document IDs and sentences as each row in a table
'''

'''
def readSources():
   result = {}
   coca_dir = os.listdir('COCAcsv/')
   for filename in coca_dir:
      o_file = 'COCAcsv/' + filename
      doctype = o_file.split('_')[-1][:-4]
      with codecs.open(o_file, 'r', encoding='utf-8', errors='ignore') as f:
         formatList = []
         fReader = csv.DictReader(f, delimiter=',')
         fdict = {row['textID']: (doctype, row['Year']) for row in fReader}
         result.update(fdict)   
   return result
'''

def readSources():
   result = {}
   o_file = '/lib/466/COCA/coca-sources.txt'
   with codecs.open(o_file, 'r', encoding='utf-8', errors='ignore') as f:
      i = 1
      colNames = []
      for line in f:
         if i==1:  
            line = line.replace('\n', '')
            colNames= re.split('\t+', line)
         elif i >= 4:
            line = line.replace('\n', '')
            fields= line.split('\t')
            row = dict(zip(colNames, fields))
            fdict = {row['textID']: (row['genre'], row['year'])}
            result.update(fdict) 
         i = i +1
   return result

def readLexiconFile():
   result = []
   #o_file = 'COCA/coca-samples-lexicon.txt'
   o_file = '/lib/466/COCA/coca_lexicon.txt'
   with open(o_file, 'r') as f:
      i = 1
      colNames = []
      for line in f:
         if i==1:  
            line = line.replace('\n', '')
            colNames= line.split('\t')
         elif i >= 4:
            line = line.replace('\n', '')
            fields= line.split('\t')
            row = dict(zip(colNames, fields))
            result.append(row)
         i = i +1
   return result

def main():
    
    #coca_dir = os.listdir('COCA/text/')
    coca_dir = os.listdir('/lib/466/COCA/text_unzipped/')

    lines = []   
   
    textid_dict = readSources()
 
    with open('coca_full_table.csv', 'w', encoding='utf8',newline='')as coca_csv:
        coca_write = csv.writer(coca_csv)
        
        for filename in coca_dir:
            #o_file = 'COCA/text/' + filename
            o_file = '/lib/466/COCA/text_unzipped/' + filename
            type = ""
            year = ""
            fName = filename.split('.')[0]
            fNameParts = fName.split('_')
            if len(fNameParts) == 2:
               year = fNameParts[0]
               type = fNameParts[1].upper()
            else:
               year = fNameParts[2]
               type = fNameParts[1].upper()
            with open(o_file, 'r') as f:
                for line in f:
                    if not line == '\n':
                        line = line.split(' ')
                        #text_id = line[0].lstrip('#')
                        text_id = ''.join([x for x in list(line[0]) if x.isdigit()])
                        #print(line[0])
                    
                        textLine = ' '.join(line[1:])
                        textLine = textLine.replace('!','.')
                        textLine = textLine.replace('?','.')
                        textLine = textLine.replace(',','<comma>')
                        textLine = textLine.replace(' <p> ',' ')
                        textLine = textLine.replace(' @ @ @ @ @ @ @ @ @ @ ', ' ')
                        textLine = textLine.replace('\n','')
                        textLine = textLine.replace('\r','')
                        sentences = textLine.split(' . ')
                        
                        
                        idFound = text_id in textid_dict.keys()
                    
                        for sentence in sentences:
                            sentence = sentence.replace(' @ @ @ @ @ @ @ @ @ @', '')
                            coca_write.writerow([text_id, textid_dict[text_id][0] if idFound else type, textid_dict[text_id][1] if idFound else year,sentence])


if __name__ == '__main__':
    main()
