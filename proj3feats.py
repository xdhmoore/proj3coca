#!/usr/bin/env python3

import argparse
import csv
import glob
import nltk
import os
import re
import pickle
import random
from nltk.corpus import stopwords
from nltk.tokenize import WordPunctTokenizer
from proj3logging import MyLogger
from nltk.classify.scikitlearn import SklearnClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.naive_bayes import BernoulliNB
from sklearn.naive_bayes import MultinomialNB
from sklearn.tree import DecisionTreeClassifier

MyLogger.off()

THIS_DIR = os.path.dirname(os.path.realpath(__file__))
DATA_DIR = THIS_DIR + '/data/'
SAMPLE_DIR = '/home/jacobs466/finalProject/data/'
#SAMPLE_DIR = DATA_DIR + 'samples/'
FEATURE_DIR = DATA_DIR + 'features/'
CLASS_DIR = DATA_DIR + 'classifiers/'
MAIN_CLASSIFIER = CLASS_DIR + "service_classifier.clf"

def readLexiconFile():
   result = []
   #o_file = 'COCA/coca-samples-lexicon.txt'
   o_file = '/lib/466/COCA/coca_lexicon.txt'
   with open(o_file, 'r') as f:
      i = 1
      colNames = []
      for line in f:
         if i==1:  
            line = line.replace('\n', '')
            colNames= line.split('\t')
         elif i >= 4:
            line = line.replace('\n', '')
            fields= line.split('\t')
            row = dict(zip(colNames, fields))
            result.append(row)
         i = i +1
   return result

def get_classifier():
    with open(MAIN_CLASSIFIER, 'rb') as f:
        classifier = pickle.load(f)
        print('Unpickled classifier ' + MAIN_CLASSIFIER)
        return classifier


def get_classifier_ensemble():
    pickles = sorted(glob.glob(CLASS_DIR + '*.clsf'))
    ensemble = []

    for classifier in pickles[:10]:
        filename = classifier
        with open(filename, 'rb') as f:
            opened_classifier = pickle.load(f)
            ensemble.append(opened_classifier)
            print('Unpickled classifier ' + classifier)

    return ensemble


"""
Tests the accuracy of an ensemble of classifiers.
Like NLTK, takes in tuples in the form of (features, class)
"""
def ensemble_accuracy(tuples, ensemble):
    num_correct = 0
    num_incorrect = 0
    confs = []

    for item in tuples:
        classification = cocaclassify(item[0], ensemble, extractfeat=False)
        confs.append(classification[1])

        if classification[0] == item[1]:
            num_correct += 1
        else:
            num_incorrect += 1

    avgconf = sum(confs) / len(confs)
    print("Avg. confidence: " + str(avgconf))
    return num_correct / len(tuples)

def cocaclassify(doc, classifier, extractfeat=True):
    if extractfeat:
        feature_dict = get_features(doc)
    else:
        feature_dict = doc
    return classifier.classify(feature_dict), None


def cocaclassify_ensemble(doc, ensemble, extractfeat=True):
    #lexicon = readLexiconFile()
    if extractfeat:
        feature_dict = get_features(doc)
    else:
        feature_dict = doc
    votes = []

    for classifier in ensemble:
        votes.append(classifier.classify(feature_dict))

    votes = sorted(votes, key=votes.count, reverse=True)
    conf = votes.count(votes[0]) / len(ensemble)

    return votes[0], conf


def isNonAlNum(text):
   result = True
   i =0
   while result and i<len(text):
      curChar = list(text)[i]
      result = (not (curChar.isalnum()))
      i = i+1
   return result

def get_features(sentence):
    feat_dict = {}

    token_count = 0
    
    stopWords = set(stopwords.words('english'))
    word_punct_tokenizer = WordPunctTokenizer()
   
    tokens = word_punct_tokenizer.tokenize(sentence)
    tokens = [word.lower() for word in tokens if len(word) > 5 and not isNonAlNum(word)] 
    tokens = list(set(word_punct_tokenizer.tokenize(sentence)) - stopWords)
   
    ''' 
    # Bag of words
    for token in tokens:
       if isNonAlNum(token) == False and len(token) >= 6:
          feat_name = 'has_' + token.lower()
          feat_dict[feat_name] = True
    '''

    word_bigrams = nltk.bigrams(tokens)
    for bg in word_bigrams:
       feat_dict[bg] = True     
        
    return feat_dict


def create_classifier(filename, loadfeats=False):

    if loadfeats and not os.path.isfile(FEATURE_DIR + filename + '.ftr'):
        print("WARNING: File '" + filename + ".ftr' does not exist  Extracting features from data set.")
        loadfeats = False

    if loadfeats:
        print("Found feature file '" + FEATURE_DIR + filename + ".ftr'. Loading features from file.")
        with open(FEATURE_DIR + filename + '.ftr', 'rb') as f:
            featureset = pickle.load(f)
    else:
        dataset = []
        i = 1
        with open(SAMPLE_DIR + filename, 'r') as f:
            csv_reader = csv.reader(f)
            
            for row in csv_reader:
                dataset.append(row)
                if i%100000 == 0:
                   print("{0}K rows of 30500K".format(i/1000))
                i = i+1
        print("read csv")
        #lexicon = readLexiconFile()
        #featureset = [(get_features(sentence[3]), sentence[2]) for sentence in dataset]
        featureset = []
        i = 1
        for sentence in dataset:
           featureset.append((get_features(sentence[3]), sentence[2]))
           if i%100000 == 0:
              print("{0}K sentences of 30500K".format(i/1000))
           i = i+1
        print("saving features")
        with open(FEATURE_DIR + filename + '.ftr', 'wb') as f:
            pickle.dump(featureset, f)
        print("feature extraction complete")
            

    random.shuffle(featureset)
    cutoff = len(featureset) // 3
    test = featureset[:cutoff]
    train = featureset[cutoff:]

    print("created test sets")

    #classifier = nltk.NaiveBayesClassifier.train(train)

    #scikitclassifier = BernoulliNB()
    scikitclassifier = MultinomialNB()
    #weak = DecisionTreeClassifier(max_depth=5)
    #scikitclassifier = AdaBoostClassifier(n_estimators=50,base_estimator=weak)
    classifier = SklearnClassifier(scikitclassifier, dtype=bool).train(train)


    print("created classifier")
    MyLogger.debug(classifier)

    print(nltk.classify.accuracy(classifier, test)) 

    #with open(CLASS_DIR + filename + ".clsf", 'wb') as f:
    with open(MAIN_CLASSIFIER, 'wb') as f:
        pickle.dump(classifier, f)
    print("dumped classifier to file")
    return classifier




def setupargs():
   parser = argparse.ArgumentParser(description="Classifier Creation")
   parser.add_argument("-f", action="store_true", dest="loadfeats",
         help="Load the features from feats directory instead of extracting them")

   return parser.parse_args()


def main():

    args = setupargs()

    test = []
    #filename = 'coca_table_sample.csv'
    filename = 'coca_full_table_part01'
    for f in glob.glob(CLASS_DIR + "*.clsf"):
        os.remove(f)
    classifier = create_classifier(filename, args.loadfeats)
    
    #print(classifier.show_most_informative_features(10))

if __name__ == '__main__':
    main()
