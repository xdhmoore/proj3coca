#!/usr/bin/env python3

import inspect
import logging
import sys

class AbstractLogger: #{{{
    active = False
    showstack = False

    @classmethod
    def on(cls):
        cls.active = True

    @classmethod
    def off(cls):
        cls.active = False
#}}}

class MyLogger(AbstractLogger): #{{{
    baseline = len(inspect.stack())
    log = logging.getLogger('proj3')
    logging.basicConfig()
    log.setLevel(logging.DEBUG)

    @classmethod
    def debug(cls, msg, *args):
        if MyLogger.active:
            if MyLogger.showstack:
                fname = sys._getframe(1).f_code.co_name
                indent = len(inspect.stack()) - MyLogger.baseline
                logArgs = ['|' * indent + ":" + fname + ":" + str(msg)]
            else:
                logArgs = [str(msg)]
            logArgs.extend(args)
            MyLogger.log.debug(*logArgs)
#}}}




