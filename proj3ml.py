#!/usr/local/bin/python3
import sys,os,re
from numpy import array
import random,operator,nltk
import unicodedata
import string
import csv

def getOneColValues(result,target):
   colVals = []
   for row in result:
      val = str(row[target])
      if val not in colVals:
         colVals.append(val)
   return colVals

def getOneColCounts(result,target):
   colCounts = {}
   for row in result:
      key = str(row[target])
      if key in colCounts:
         colCounts[key] = colCounts[key] + 1.0
      else:
         colCounts[key] = 1.0
   return colCounts

def getTwoColCounts(result,target,other,tVals,oVals):
   colCounts = {}
   for val1 in oVals:
      for val2 in tVals:
         key = val1+","+val2
         colCounts[key] = 0.0
         for row in result:
            if str(row[other]) == val1 and str(row[target]) == val2:
               colCounts[key] = colCounts[key] + 1.0
   return colCounts
   
def CalcBayes(total,target,groupCounts,colVals,colNames):
   pResults = {}
   for key,value in target.items():
      pOverall=value/total
      i=0
      while i<len(colVals):
         groupKey = colVals[i] + "," + key
         pGroup = groupCounts[colNames[i]][groupKey] / value
         pOverall= pOverall*pGroup
         i=i+1
      pResults[key] = pOverall
   print(pResults)
   
   

def getFileLines(filename):
   content = []
   with open(fname) as f:
      content = f.readlines()
   content = [x[:-1] for x in content]
   return content

def main():
   getFileLines("")   
   colNames = list(result[0].keys())
   totalCount = 0.0
   targetCount = {}
   colNames = []
   colSingleCounts = {}
   colGroupCounts = {}
   result = [] #data from file 
   colNames.remove(target)
   colNames.sort()
   totalCount = float(len(result))
   targetCount = getOneColCounts(result, target)
   print(targetCount)
   for col in colNames:
     valCountDict = getOneColCounts(result, col)
     colSingleCounts[col]=valCountDict
     condCountDict = getTwoColCounts(result, target, col, targetCount.keys(), valCountDict.keys())
     colGroupCounts[col]=condCountDict
   CalcBayes(totalCount, targetCount, colGroupCounts, colValues, colNames)

if __name__ == "__main__":
    main()