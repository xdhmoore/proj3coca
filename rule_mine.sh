#!/bin/bash

DIR=`ls rules/rule*`

for i in $DIR; do
    YEAR=`echo $i | cut -d '_' -f 2`
    OFILE="rules/random_"$YEAR"_news.csv"
    echo $OFILE
    sort -R $i | head -n 4000 > $OFILE
done

FILES=`ls rules/random*`

for i in $FILES; do
    OFILE="results/random_"`echo $i | cut -d '_' -f 2`"_news.out"

    ./apriori.py $i 0.005 0.1 > $OFILE
done
