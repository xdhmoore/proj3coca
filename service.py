#!/usr/bin/env python3

from flask import Flask, jsonify
from flask import request
from proj3feats import get_classifier, cocaclassify

app = Flask(__name__)
app.config.update(
    SERVER_NAME="localhost:5281"
)

classifier = get_classifier()

@app.route('/', methods=['POST'])
def index():
    return cocaclassify(request.json['doc'], classifier)[0]
    #return jsonify(request.json)

if __name__ == '__main__':
    app.run(debug=False)
