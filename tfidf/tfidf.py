import csv, math
import pickle, nlkt
from collections import defaultdict
from sklearn.feature_extraction.text import TfidfVectorizer


lines = defaultdict(list)

with open("/home/jacobs466/finalProject/data/coca_full_table.csv", "r") as f:
	reader = csv.reader(f)
	i = 0
	for row in reader:
		if i % 10 == 0:
			lines[row[2]].append(row[3].replace("<comma>", ''))
		i += 1

for year,text in lines.items():
	lines[year] = " ".join(text)

all_text = []
for year,text in sorted(lines.items(), key= lambda x: int(x[0])):
	all_text.append(text)

tf = TfidfVectorizer(analyzer='word', ngram_range=(1,3), min_df=0.2, stop_words='english')

print("fitting corpus. size", len(all_text))

matrix = tf.fit_transform(all_text)
feats = tf.get_feature_names()

pick = open("tfidf_feats_3mil.pkl", "wb")
pickle.dump(feats, pick)
pick.close()

pick = open("tfidf_matrix_3mil.pkl", "wb")
pickle.dump(matrix, pick)
pick.close()
