import pickle
import csv

feats = pickle.load(open("tfidf_feats_3mil.pkl", "rb"))
matrix = pickle.load(open("tfidf_matrix_3mil.pkl", "rb"))

print(len(feats))

dense_mat = matrix.todense()

with open("phrases.csv", "w") as f:
	writer = csv.writer(f, delimiter=",")
	
	year = 1990
	for doc in dense_mat:
		word_i = 0
		for score in doc.tolist()[0]:
			if score > 0.0001:
				writer.writerow([year, feats[word_i].encode("utf-8"), score])
			word_i += 1
		year += 1
